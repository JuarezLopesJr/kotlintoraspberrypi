@file:OptIn(ExperimentalTime::class)

import kotlin.random.Random
import kotlin.time.ExperimentalTime

fun main() {
//    println(Array(5) { (it + 1) * (it + 1) }.sum())

    printTransformedMessage {
        sarcastic()
    }

    /*val (value, time) = measureTimedValue {
        longRunning()
    }
    println("It took $time to calculate $value")*/

}

// extension function with receiver String
fun String.sarcastic(): String {
    return asIterable().joinToString(" ") {
        if (Random.nextBoolean()) it.uppercase() else it.lowercase()
    }
}

// lambda with receiver String
fun printTransformedMessage(transform: String.() -> Unit) {
    val greeting = "Mussum ipsum"
    val transformed = greeting.transform()
    println(transformed)
}

/*
fun longRunning(): String {
    repeat(2_000_000) {
        print("*")
    }
    return "Done"
}*/
